# Ansible Go and Google Protobuf Tools Installer

This directory contains Ansible playbooks and config files to install on VM(s):

* A Go designer environment.  
  (`go` tool, setup $GOPATH, etc.)
* The software required to compile protobuffer and gRPC services.  
  (protoc and the protoc-gen-go plugin)

**NOTE**  
This is a small repository that stores a few simple playbooks.  
It was useful to me to add Go and gRPC environment to the VMs obtained from  
<https://github.com/p4lang/tutorials/tree/master/vm>  
It does not try to achieve some very high level goals or solving world hunger.   
If it fits your requirements adapt the parameters to your environment and enjoy!

It installs Go under `/usr/local` and sets `$HOME/go` as the Go workspace ($GOPATH).  
It installs protoc compiler under `/usr/local/protoc-3.7.1`.

More could be added later

# Configuring the playbooks

The recommended way to use this repository is to clone it,   
then make a copy of the directory `my-cluster-tmpl` under a name that reflect the set of hosts you want to configure.

Set the following variables in the following files in the new directory:

* hosts  
  Name the group the way you want (e.g. `[p4-hosts]`),  
  and add al the hosts you want under the group(s) name.  
  For details see: <https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html>
  
* ansible.cfg  
  You can set various global settings there.  
  If you use ssh keys with the private key on a custom path, set the variable `private_key_file`.  
  This is typical if configuring hosts in AWS.
  
  If security is a lesser concern you can also specify the password to use with ssh  
  in the `hosts` file with the variable `ansible_ssh_pass=TheUserPassword`.  
  To use this you may also need to install `askpass` on the control host.  
  See:  
  <https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html>
  <https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html?highlight=ansible_password>
  

* group_vars/all.yaml  
  Set the user and group you want to use to determine the HOME dir and the ownership.  
  E.g. 
  ```
  install_owner: ubuntu
  install_group: ubuntu
  ```

# Running the Playbook

To run the installer, cd into the cluster dir you want to configure, and run the bash wrapper script.

```
$ cd <your cluster dir>
$ ./install-go-proto.bash
```

# TODO
* install grpc_cli  
  Prerequisite to install grpc_cli  
  See: https://github.com/grpc/grpc/blob/master/BUILDING.md  
  `sudo apt-get install build-essential autoconf libtool pkg-config`  
  Follow with git clone grpc and make (see link above)


